//
//  SentenceCardView.m
//  iCan2
//
//  Created by 陳 雍協 on 11/9/28.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import "SentenceCardView.h"

@implementation SentenceCardView
@synthesize imageView,label,card,index;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        imageView = [[CardImageView alloc] initWithFrame:CGRectMake(10, 5, 130, 130)];
        imageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BlankGrid"]];
        [imageView setHome:imageView.frame];
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [imageView.layer setMasksToBounds:YES];
            [imageView.layer setBorderWidth:1.0];
            [imageView.layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
            [imageView.layer setCornerRadius:5.0];
        }
        [self addSubview:imageView];
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(10, 140, 128, 15)];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor blackColor];
        label.adjustsFontSizeToFitWidth = YES;
        label.font = [UIFont systemFontOfSize:13];
        label.textAlignment = UITextAlignmentCenter;
        [self addSubview:label];
        
    }
    return self;
}

- (void)dealloc {
    [imageView release];
    [label release];
    [card release];
    [super dealloc];
}

-(CGPoint)getImageCenter{
    return CGPointMake(75 +150*index,75);
}

- (void)setUpCard:(Card *)aCard {
    self.card = aCard;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    self.imageView.image = card.image;
    self.label.text = card.title;
    [UIView commitAnimations];
}

@end
