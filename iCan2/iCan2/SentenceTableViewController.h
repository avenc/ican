//
//  SentenceTableViewController.h
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlaySentenceViewController.h"
@interface SentenceTableViewController : UITableViewController {
    NSMutableArray *sentenceList;
}
@property (nonatomic, retain) NSMutableArray *sentenceList;
- (void)reloadList;
@end
