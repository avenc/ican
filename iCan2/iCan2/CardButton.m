//
//  CardButton.m
//  iCan
//
//  Created by avenc on 2011/6/16.
//  Copyright 2011年 NTU MHCI LAb . All rights reserved.
//

#import "CardButton.h"


@implementation CardButton
@synthesize card,delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id) initWithCardStyle:(CardButtonStyle)aStyle andCard:(Card *)aCard{
    self = [super initWithFrame:CGRectMake(0, 0, kCardButtonWidth, kCardButtonHeight)];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        style = aStyle;
        self.card = aCard;
        button = [[UIButton alloc] initWithFrame:CGRectMake((kCardButtonWidth-190)/2, 10, 190, 190)];

        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];

        
        [self addSubview:button];
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, button.frame.origin.y + button.frame.size.height+ 2, kCardButtonHeight, 30)];
        label.opaque = NO;
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont systemFontOfSize:20.0];
        label.textAlignment = UITextAlignmentCenter;
        label.textColor = [UIColor blackColor];

        [self addSubview:label];
        switch (style) {
            case CardButtonStyleAdd:
                [button setImage:[UIImage imageNamed:@"add_card_btn_back"] forState:UIControlStateNormal];
                button.backgroundColor = [UIColor clearColor];
                break;
            case CardButtonStyleDefault:
                [button setImage:aCard.image forState:UIControlStateNormal];
                if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                    [button.layer setBorderColor:[[UIColor grayColor]CGColor]];
                    [button.layer setBorderWidth:1.0];
                    
                    [button.layer setCornerRadius:5.0];
                    [button.layer setMasksToBounds:YES];
                }
                label.text = aCard.title;
                break;                
            default:
                break;
        }

        
    }
    return self;
}


- (void)dealloc
{
    self.delegate = nil;
    [button release];
    [label release];
    [card release];
    [super dealloc];
}

- (void)buttonClicked:(id)sender {
    NSLog(@"Ker");
    switch (style) {
        case CardButtonStyleAdd:
            if ([delegate respondsToSelector:@selector(CardButtonAddDidClicked:)]) 
                [delegate CardButtonAddDidClicked:self];
            break;
        case CardButtonStyleDefault:
            if ([delegate respondsToSelector:@selector(CardButtonEditDidClicked:)]) 
                [delegate CardButtonEditDidClicked:self];
            break;
        default:
            break;
    }

}

@end
