//
//  SentenceLabelCard.m
//  iCan
//
//  Created by avenc on 2011/6/12.
//  Copyright 2011年 NTU MHCI LAb . All rights reserved.
//

#import "SentenceLabelCard.h"


@implementation SentenceLabelCard
@synthesize imageView,index,cardId,label;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCard:(Card *)aCard andIndex:(int)aIndex{
    self = [super initWithFrame:CGRectMake(0, 0, 200, 240)];
    if (self) {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 200, 200)];
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [imageView.layer setMasksToBounds:YES];
            [imageView.layer setBorderWidth:1.0];
            [imageView.layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
            [imageView.layer setCornerRadius:5.0];
        }

        imageView.image = aCard.image;
        [self addSubview:imageView];
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 215, 200, 20)];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont boldSystemFontOfSize:20.0];
        label.textAlignment = UITextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.text = aCard.title;
        [self addSubview:label];
        
    }
    return self;
}


- (void)dealloc {
    [imageView release];
    [label release];
    [super dealloc];
    
}


@end
