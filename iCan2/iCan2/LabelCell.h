//
//  LabelCell.h
//  iCan
//
//  Created by danielbas on 2011/5/19.
//  Copyright 2011年 NTU MHCI LAb . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface LabelCell : UITableViewCell {
    UILabel *contentLabel;  
    UIView *colorView;
}

@property (nonatomic, retain) UILabel *contentLabel;
@property (nonatomic, retain) UIView *colorView;
- (id)initWithIndex:(int)index reuseIdentifier:(NSString *)reuseIdentifier;
@end
