//
//  CardButton.h
//  iCan
//
//  Created by avenc on 2011/6/16.
//  Copyright 2011年 NTU MHCI LAb . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Card.h"
#define kCardButtonWidth 235
#define kCardButtonHeight 220

typedef enum {
    CardButtonStyleAdd = 0,
    CardButtonStyleDefault,
} CardButtonStyle;


@protocol CardButtonDelegate;

@interface CardButton : UIView {
    UIButton *button;
    UILabel *label;
    Card *card;
    CardButtonStyle style;
    id <CardButtonDelegate>delegate;
}
@property (nonatomic,retain)Card *card;
@property (nonatomic,assign)id <CardButtonDelegate>delegate;
- (id) initWithCardStyle:(CardButtonStyle)aStyle andCard:(Card *)aCard;
- (void)buttonClicked:(id)sender;


@end


@protocol CardButtonDelegate <NSObject>
@optional
-(void)CardButtonEditDidClicked:(CardButton*)cardButton;
-(void)CardButtonAddDidClicked:(CardButton*)cardButton;

@end