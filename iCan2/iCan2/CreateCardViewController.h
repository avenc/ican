//
//  CreateCardViewController.h
//  iCan2
//
//  Created by 陳 雍協 on 11/10/6.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Card.h"
#import "CategorySelectionView.h"
#import <AVFoundation/AVFoundation.h>
#import "DrawViewController.h"

@protocol CreateCardViewControllerDelegate; 

@interface CreateCardViewController : UIViewController <UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CategorySelectionViewDelegate,UIAlertViewDelegate,AVAudioRecorderDelegate,AVAudioPlayerDelegate,DrawViewControllerDelegate,UITextFieldDelegate>{
    id <CreateCardViewControllerDelegate>delegate;
    UIPopoverController *popOver;
    Card *card;
    UIImageView *imageView;
    UIButton *categoryBtn;
    UITextField *nameField;
    UIButton *playBtn;
    UIButton *recordBtn;
    UIButton *cancleBtn;
    UIButton *finishBtn;    
    
    UITextField *categoryField;
    UIImage *captureImage;
    NSString *newName;    
    
    AVAudioRecorder *recorder;
    AVAudioPlayer *audioPlayer;
    NSString *recorderFilePath;
    BOOL isPlaying;
    BOOL isRecording;
    BOOL isPlayerAvailable;
    NSData *recordData;
    NSMutableDictionary *recordSetting;
    NSString *fileName;
    
}
@property (nonatomic, assign) id <CreateCardViewControllerDelegate>delegate;
@property (nonatomic, retain) Card *card;
@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) IBOutlet UIButton *categoryBtn;
@property (nonatomic, retain) IBOutlet UITextField *nameField;
@property (nonatomic, retain) IBOutlet UIButton *playBtn;
@property (nonatomic, retain) IBOutlet UIButton *recordBtn;
@property (nonatomic, retain) IBOutlet UIButton *cancleBtn;
@property (nonatomic, retain) IBOutlet UIButton *finishBtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andCard:(Card *)aCard;
- (IBAction)categoryButtonDidClicked:(id)sender;
- (IBAction)playButtonDidClicked:(id)sender;
- (IBAction)recordButtonDidClicked:(id)sender;
- (IBAction)cancleButtonDidClicked:(id)sender;
- (IBAction)finishButtonDidClicked:(id)sender;
- (void)setUpCard:(Card *)aCard;
- (void)imageViewDidTapped:(id)sender;

#pragma mark record Control
- (void) startRecording;
- (void) stopRecording;
- (void) playSound;
- (void) stopSound;
@end

@protocol CreateCardViewControllerDelegate <NSObject>
@optional 
- (void)createCardViewController:(CreateCardViewController*)vc finishedAddingCard:(Card *)aCard;
- (void)createCardViewController:(CreateCardViewController*)vc failededAddingCard:(Card *)aCard;
@end