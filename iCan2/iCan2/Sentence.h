//
//  Sentence.h
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sentence : NSObject {
    NSNumber *sid;
    NSNumber *playTimes;
    NSDate *createTime;
    NSArray *cardArray;
    NSString *title;
}
@property (nonatomic,retain)NSNumber *sid;
@property (nonatomic,retain)NSNumber *playTimes;
@property (nonatomic,retain)NSDate *createTime;
@property (nonatomic,retain)NSArray *cardArray;
@property (nonatomic,retain)NSString *title;

- (id)initWithCards:(NSArray *)cards;

@end
