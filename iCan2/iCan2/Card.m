//
//  Card.m
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import "Card.h"

@implementation Card
@synthesize cid;
@synthesize title;
@synthesize soundPath;
@synthesize category;
@synthesize imagePath;
@synthesize soundData;
@synthesize image;


-(void)dealloc {
    [cid release];
    [title release];
    [soundPath release];
    [category release];
    [imagePath release];
    [soundData release];
    [image release];
    [super dealloc];
}

@end
