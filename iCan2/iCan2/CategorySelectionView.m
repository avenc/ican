//
//  CategorySelectionView.m
//  iCan2
//
//  Created by 陳 雍協 on 11/10/10.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import "CategorySelectionView.h"
#import "LabelCell.h"

@implementation CategorySelectionView
@synthesize categoryList,delegate;
- (id)initWithStyle:(UITableViewStyle)style andCategory:(NSArray *)array andSelectIndex:(int)index
{
    self = [super initWithStyle:style];
    if (self) {
        self.categoryList = array;
        selectedIndex = index;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = kBackGroundColor;
    self.clearsSelectionOnViewWillAppear = NO;
}
- (void)dealloc {
    self.delegate = nil;
    [categoryList release];
    [super dealloc];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [categoryList count] +1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    LabelCell *cell = (LabelCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[LabelCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    cell.colorView.backgroundColor = [kCategoryColorArray objectAtIndex:(indexPath.row%[kCategoryColorArray count])];
    
    
    if (indexPath.row<[categoryList count]) {
        cell.contentLabel.text = [categoryList objectAtIndex:indexPath.row];
        [cell.contentLabel setTextColor:[UIColor blackColor]];
        if (indexPath.row == selectedIndex) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    } else {
        cell.contentLabel.text = @"﹢ 新增類別";
        cell.colorView.backgroundColor = [UIColor clearColor];
        [cell.contentLabel setTextColor:[UIColor redColor]];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    UIView* myBackgroundView = [[UIView alloc] initWithFrame:CGRectZero] ;
    if (indexPath.row%2 == 0) {
        cell.contentView.backgroundColor = [UIColor colorWithRed:0.867 green:0.886 blue:0.91 alpha:1.0];
        cell.backgroundView = myBackgroundView;
        cell.backgroundView.backgroundColor = [UIColor colorWithRed:0.867 green:0.886 blue:0.91 alpha:1.0];
        [myBackgroundView release];
    } else {
        cell.contentView.backgroundColor = [UIColor colorWithRed:0.925 green:0.925 blue:0.945 alpha:1.0];    
        cell.backgroundView = myBackgroundView;
        cell.backgroundView.backgroundColor = [UIColor colorWithRed:0.925 green:0.925 blue:0.945 alpha:1.0];      
        [myBackgroundView release];        
    }
    
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedIndex = indexPath.row;
    [tableView reloadData];
    
    if (indexPath.row == [categoryList count]) {
        if ([delegate respondsToSelector:@selector(categorySelectionViewDidSeleteAddCategory:)]) {
            [delegate categorySelectionViewDidSeleteAddCategory:self];
        }
    } else if (indexPath.row < [categoryList count]) {
        if ([delegate respondsToSelector:@selector(categorySelectionView:didSeleteCategory:)]) {
            [delegate categorySelectionView:self didSeleteCategory:[categoryList objectAtIndex:indexPath.row]];
        }
    }

}

@end
