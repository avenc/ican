//
//  DataBaseManager.h
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "Card.h"
#import "Sentence.h"

@interface DataBaseManager : NSObject {
    FMDatabase* db;
}
+(DataBaseManager *)defaultDataBaseManager;
+(void)releaseDataBaseManager;

-(NSArray *)getAllCard;
-(NSArray *)getAllCategory;
-(NSArray *)getCardWithCategory:(NSString *)category;
-(Card *)getCardDicWithCardId:(NSNumber *)card_Id;
-(BOOL)deleteCardWithCardId:(NSNumber *)card_Id;
-(BOOL)addCardIntoDataBase:(Card *)aCard;
-(BOOL)updateCard:(Card *)aCard;
-(BOOL)addSentenceToDB:(Sentence *)aSentence;
-(NSArray *)getAllSentence;
-(BOOL)deleteSentenceWithSid:(NSNumber *)sentence_Id;
@end
