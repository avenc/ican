//
//  Sentence.m
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import "Sentence.h"
#import "Card.h"
@implementation Sentence
@synthesize sid;
@synthesize playTimes;
@synthesize createTime;
@synthesize cardArray;
@synthesize title;

- (id)initWithCards:(NSArray *)cards {
    self = [super init];
    if (self) {
        self.playTimes = 0;
        self.createTime = [NSDate date];
        self.cardArray = cards;
        NSMutableString *temptitle = [NSMutableString stringWithString:@""];
        for (int i = 0; i < [cards count] ; i++) {
            Card *card = (Card *)[cards objectAtIndex:i];
            [temptitle appendString:card.title];
        }
        self.title = temptitle;
    }
    return self;
}

- (void)dealloc {
    [title release];
    [sid release];
    [playTimes release];
    [createTime release];
    [cardArray release];
    [super dealloc];
}

@end
