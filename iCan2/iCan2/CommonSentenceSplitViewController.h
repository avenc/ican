//
//  CommonSentenceSplitViewController.h
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SentenceTableViewController.h"
#import "PlaySentenceViewController.h"
@interface CommonSentenceSplitViewController : UISplitViewController {
    SentenceTableViewController *leftViewController;
    PlaySentenceViewController *rightViewController;
}
@property (nonatomic, retain) SentenceTableViewController *leftViewController;
@property (nonatomic, retain) PlaySentenceViewController *rightViewController;

@end
