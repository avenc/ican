//
//  CardManageSplitViewController.h
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardManageViewController.h"
#import "CradManageTableViewController.h"
@interface CardManageSplitViewController : UISplitViewController {
//    UISplitViewController *splitViewController;
    CradManageTableViewController *leftViewController;
    CardManageViewController *rightViewController;
}
//@property (nonatomic, retain) UISplitViewController *splitViewController;
@property (nonatomic, retain) CradManageTableViewController *leftViewController;
@property (nonatomic, retain) CardManageViewController *rightViewController;
@end
