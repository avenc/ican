//
//  CategorySelectionView.h
//  iCan2
//
//  Created by 陳 雍協 on 11/10/10.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CategorySelectionViewDelegate;

@interface CategorySelectionView : UITableViewController {
    id <CategorySelectionViewDelegate>delegate;
    NSArray *categoryList;
    int selectedIndex;
}
@property (nonatomic, retain) id <CategorySelectionViewDelegate>delegate;
@property (nonatomic, retain) NSArray *categoryList;
- (id)initWithStyle:(UITableViewStyle)style andCategory:(NSArray *)array andSelectIndex:(int)index;
@end


@protocol CategorySelectionViewDelegate <NSObject>
@optional
- (void)categorySelectionView:(CategorySelectionView *)vc didSeleteCategory:(NSString *)categoryName;
- (void)categorySelectionViewDidSeleteAddCategory:(CategorySelectionView *)vc;
@end