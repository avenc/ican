//
//  DrawViewController.h
//  iCan2
//
//  Created by 陳 雍協 on 11/10/10.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DrawViewControllerDelegate;

@interface DrawViewController : UIViewController {
    id<DrawViewControllerDelegate> delegate;
    CGPoint lastPoint;
	BOOL mouseSwiped;	
	int mouseMoved;
    UIImageView *imageView;
    NSMutableArray *pointsArray;
}

@property (nonatomic,retain) UIImageView *imageView;
@property (nonatomic,assign) id<DrawViewControllerDelegate> delegate;
-(void)clear:(id)sender;
-(void)finish:(id)sender;
@end

@protocol DrawViewControllerDelegate <NSObject>
@optional
- (void)drawViewController:(DrawViewController*)drawView getImage:(UIImage *)image;

@end