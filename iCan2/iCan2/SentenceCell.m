//
//  SentenceCell.m
//  iCan
//
//  Created by avenc on 2011/6/12.
//  Copyright 2011年 NTU MHCI LAb . All rights reserved.
//

#import "SentenceCell.h"


@implementation SentenceCell
@synthesize thumbImage,label,sentence;

- (id)initWithreuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        thumbImage = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, 40, 40)];

        [self.contentView addSubview:thumbImage];
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(thumbImage.frame.origin.x +thumbImage.frame.size.width + 5, 10, 265, 40)];
        label.font = [UIFont boldSystemFontOfSize:20.0f];
        label.adjustsFontSizeToFitWidth = NO;
        label.numberOfLines = 0;
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = UITextAlignmentLeft;
        label.textColor = [UIColor blackColor];
        [self.contentView addSubview:label];
        
        self.accessoryType = UITableViewCellAccessoryNone;
        self.selectionStyle = UITableViewCellSelectionStyleGray;
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [thumbImage.layer setMasksToBounds:YES];
            [thumbImage.layer setBorderWidth:1.0];
            [thumbImage.layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
            [thumbImage.layer setCornerRadius:5.0];
        }
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setUpCell:(Sentence *)aSentence{
    self.sentence = aSentence;
    Card *firstCard = [sentence.cardArray objectAtIndex:0];
    thumbImage.image = firstCard.image;
    label.frame = CGRectMake(label.frame.origin.x, label.frame.origin.y, label.frame.size.width, [self getLabelHeight]);
    label.text = sentence.title;
}

- (void)dealloc {
    [sentence release];
    [thumbImage release];
    [label release];
    [super dealloc];
}

- (CGFloat)getLabelHeight{
    CGSize constraintSize = CGSizeMake(265, MAXFLOAT);
    CGSize labelSize = [sentence.title sizeWithFont:[UIFont boldSystemFontOfSize:20.0f] constrainedToSize:constraintSize lineBreakMode:UILineBreakModeCharacterWrap];
    return labelSize.height ;
}


+ (CGFloat)getCellHeight:(NSString*)str{
    CGSize constraintSize = CGSizeMake(265, MAXFLOAT);
    CGSize labelSize = [str sizeWithFont:[UIFont boldSystemFontOfSize:20.0f] constrainedToSize:constraintSize lineBreakMode:UILineBreakModeCharacterWrap];
    if (labelSize.height + 20 < 60) {
        return 60;
    } 
    return labelSize.height + 20;
}

@end
