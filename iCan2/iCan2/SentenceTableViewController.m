//
//  SentenceTableViewController.m
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import "SentenceTableViewController.h"
#import "DataBaseManager.h"
#import "Sentence.h"
#import "SentenceCell.h"

@implementation SentenceTableViewController
@synthesize sentenceList;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self reloadList];
    self.view.backgroundColor = kBackGroundColor;
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Sentence *sentence = [sentenceList objectAtIndex:indexPath.row];
    return [SentenceCell getCellHeight:sentence.title];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [sentenceList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    SentenceCell *cell = (SentenceCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[SentenceCell alloc] initWithreuseIdentifier:CellIdentifier] autorelease];
    }
    [cell setUpCell:[sentenceList objectAtIndex:indexPath.row]];  
    
    UIView* myBackgroundView = [[UIView alloc] initWithFrame:CGRectZero] ;
    if (indexPath.row%2 == 0) {
        cell.contentView.backgroundColor = [UIColor colorWithRed:0.867 green:0.886 blue:0.91 alpha:1.0];
        cell.backgroundView = myBackgroundView;
        cell.backgroundView.backgroundColor = [UIColor colorWithRed:0.867 green:0.886 blue:0.91 alpha:1.0];
        [myBackgroundView release];
    } else {
        cell.contentView.backgroundColor = [UIColor colorWithRed:0.925 green:0.925 blue:0.945 alpha:1.0];    
        cell.backgroundView = myBackgroundView;
        cell.backgroundView.backgroundColor = [UIColor colorWithRed:0.925 green:0.925 blue:0.945 alpha:1.0];      
        [myBackgroundView release];        
    }
    
    return cell;
}

- (void)reloadList {
    self.sentenceList = [NSMutableArray arrayWithArray:[[DataBaseManager defaultDataBaseManager] getAllSentence]];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row<[sentenceList count]) {
        UINavigationController *nvc =(UINavigationController*) [self.splitViewController.viewControllers objectAtIndex:1];
        PlaySentenceViewController *vc = (PlaySentenceViewController*)[nvc.viewControllers objectAtIndex:0];
        [vc reloadDataWithSentence:[sentenceList objectAtIndex:indexPath.row]];
    }

}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Sentence *sentence = (Sentence *)[sentenceList objectAtIndex:indexPath.row];
        if ([[DataBaseManager defaultDataBaseManager] deleteSentenceWithSid:sentence.sid]) {
            [sentenceList removeObjectAtIndex:indexPath.row];
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
            UINavigationController *nav = [self.splitViewController.viewControllers objectAtIndex:1];
            PlaySentenceViewController *playView = [nav.viewControllers objectAtIndex:0];
            if ([sentenceList count]>0) [playView reloadDataWithSentence:[sentenceList objectAtIndex:0]];
        }
    }
    
}

@end
