//
//  SentenceCardView.h
//  iCan2
//
//  Created by 陳 雍協 on 11/9/28.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardView.h"

@interface SentenceCardView : UIView {
    CardImageView *imageView;
    UILabel *label;
    Card *card;
    int index;
}
@property(nonatomic,retain)CardImageView *imageView;
@property(nonatomic,retain)UILabel *label;
@property(nonatomic,assign)int index;
@property(nonatomic,retain)Card *card;
- (CGPoint)getImageCenter;
- (void)setUpCard:(Card *)aCard;
@end
