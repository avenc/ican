//
//  DataBaseManager.m
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import "DataBaseManager.h"
#import "JSONKit.h"
static DataBaseManager *defaultDataBaseManager = nil;

@implementation NSMutableArray (Reverse)

- (void)reverse {
    NSUInteger i = 0;
    NSUInteger j = [self count] - 1;
    while (i < j) {
        [self exchangeObjectAtIndex:i
                  withObjectAtIndex:j];
        
        i++;
        j--;
    }
}

@end

@implementation DataBaseManager

#pragma mark -
#pragma mark Singleton

+ (DataBaseManager *)defaultDataBaseManager {
    if (defaultDataBaseManager==nil) {
        @synchronized ([DataBaseManager class]) {
            if (defaultDataBaseManager==nil)
                defaultDataBaseManager = [[DataBaseManager alloc] init];
        }
    }
    return defaultDataBaseManager;
}

+ (void)releaseDataBaseManager {
    @synchronized ([DataBaseManager class]) {
        [defaultDataBaseManager release];
    }
}

- (id)init
{
    self = [super init];
    if (self) {
        NSString *dbPathInAppDocuments = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"db.sqlite"];
        db = [FMDatabase databaseWithPath:dbPathInAppDocuments];
        [db open];
        [db retain];
    }
    
    return self;
}


- (void)dealloc {
    [db close];
    [db release];
    [super dealloc];
}


-(NSArray *)getAllCard {
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    FMResultSet *rs = [db executeQuery:@"select * from Card"];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    while ([rs next]) {
        
        Card *card = [[Card alloc] init];
        
        int cid = [rs intForColumn:@"id"];
        card.cid = [NSNumber numberWithInt:cid];        
        
        NSString *title = [rs stringForColumn:@"title"];
        card.title = title;
        
        NSString *sound_path = nil;
        if (![rs columnIsNull:@"sound_path"]) {
            sound_path = [rs stringForColumn:@"sound_path"];
            if ([sound_path hasPrefix:@"file_"]) {
                sound_path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:sound_path]; 
            } else {
                sound_path = [[NSBundle mainBundle] pathForResource:sound_path ofType:nil];
            }
            card.soundData = [NSData dataWithContentsOfFile:sound_path];
            sound_path = [sound_path lastPathComponent];
        } else {
            sound_path = (NSString*)[NSNull null];
        }
        card.soundPath = sound_path;
        
        NSString *img_on = nil;
        if (![rs columnIsNull:@"img_on"]) {
            img_on = [rs stringForColumn:@"img_on"];
            if ([img_on hasPrefix:@"file_"]) {
                img_on = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:img_on]; 
            } else {
                img_on = [[NSBundle mainBundle] pathForResource:img_on ofType:nil];
            }
            card.image = [UIImage imageWithData:[NSData dataWithContentsOfFile:img_on]];
            img_on = [img_on lastPathComponent];
        } else {
            img_on = (NSString*)[NSNull null];
        }
        card.imagePath = img_on;
        
        NSString *category = [rs stringForColumn:@"category"];
        card.category = category;
        
        if (![card.soundPath isEqual:[NSNull null]] && ![card.imagePath isEqual:[NSNull null]] && card.image != nil) {
            [tempArray addObject:card];
        }
        
        [card release];
    }
    
    [pool drain];
    return [tempArray autorelease];
}

-(NSArray *)getAllCategory {
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    FMResultSet *rs = [db executeQuery:@"select category from Card group by category"];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    while ([rs next]) {
        NSString *category = [rs stringForColumn:@"category"];
        if ([category length]>0) 
            [tempArray addObject:category];
    }
    [pool drain];
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:[tempArray count]];
    for (int i = [tempArray count]-1; i>=0; i--) {
        [result addObject:[tempArray objectAtIndex:i]];
    }
    [tempArray release];
    
    return [NSArray arrayWithArray:result];
}

-(NSArray *)getCardWithCategory:(NSString *)category {
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    FMResultSet *rs = [db executeQuery:@"select * from Card where category = ?",category];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    while ([rs next]) {
        
        Card *card = [[Card alloc] init];
        
        int cid = [rs intForColumn:@"id"];
        card.cid = [NSNumber numberWithInt:cid];        
        
        NSString *title = [rs stringForColumn:@"title"];
        card.title = title;
        
        NSString *sound_path = nil;
        if (![rs columnIsNull:@"sound_path"]) {
            sound_path = [rs stringForColumn:@"sound_path"];
            if ([sound_path hasPrefix:@"file_"]) {
                sound_path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:sound_path]; 
            } else {
                sound_path = [[NSBundle mainBundle] pathForResource:sound_path ofType:nil];
            }
            card.soundData = [NSData dataWithContentsOfFile:sound_path];
            sound_path = [sound_path lastPathComponent];
        } else {
            sound_path = (NSString*)[NSNull null];
        }
        card.soundPath = sound_path;
        
        NSString *img_on = nil;
        if (![rs columnIsNull:@"img_on"]) {
            img_on = [rs stringForColumn:@"img_on"];
            if ([img_on hasPrefix:@"file_"]) {
                img_on = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:img_on]; 
            } else {
                img_on = [[NSBundle mainBundle] pathForResource:img_on ofType:nil];
            }
            card.image = [UIImage imageWithData:[NSData dataWithContentsOfFile:img_on]];
            img_on = [img_on lastPathComponent];
        } else {
            img_on = (NSString*)[NSNull null];
        }
        card.imagePath = img_on;
        
        NSString *category = [rs stringForColumn:@"category"];
        card.category = category;
        
        if (![card.soundPath isEqual:[NSNull null]] && ![card.imagePath isEqual:[NSNull null]] && card.image != nil) {
            [tempArray addObject:card];
        }
        [card release];
    }
    [pool drain];
    
    return [tempArray autorelease];
}

-(Card *)getCardDicWithCardId:(NSNumber *)card_Id {
    FMResultSet *rs = [db executeQuery:@"select * from Card where id = ?",card_Id];
    Card *card = [[Card alloc] init];
    while ([rs next]) {
        int cid = [rs intForColumn:@"id"];
        card.cid = [NSNumber numberWithInt:cid];        
        
        NSString *title = [rs stringForColumn:@"title"];
        card.title = title;
        
        NSString *sound_path = nil;
        if (![rs columnIsNull:@"sound_path"]) {
            sound_path = [rs stringForColumn:@"sound_path"];
            if ([sound_path hasPrefix:@"file_"]) {
                sound_path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:sound_path]; 
            } else {
                sound_path = [[NSBundle mainBundle] pathForResource:sound_path ofType:nil];
            }
            card.soundData = [NSData dataWithContentsOfFile:sound_path];
            sound_path = [sound_path lastPathComponent];
        } else {
            sound_path = (NSString*)[NSNull null];
        }
        card.soundPath = sound_path;
        
        NSString *img_on = nil;
        if (![rs columnIsNull:@"img_on"]) {
            img_on = [rs stringForColumn:@"img_on"];
            if ([img_on hasPrefix:@"file_"]) {
                img_on = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:img_on]; 
            } else {
                img_on = [[NSBundle mainBundle] pathForResource:img_on ofType:nil];
            }
            card.image = [UIImage imageWithData:[NSData dataWithContentsOfFile:img_on]];
            img_on = [img_on lastPathComponent];
        } else {
            img_on = (NSString*)[NSNull null];
        }
        card.imagePath = img_on;
        
        NSString *category = [rs stringForColumn:@"category"];
        card.category = category;        
    }
    return [card autorelease];
}

-(BOOL)deleteCardWithCardId:(NSNumber *)card_Id {
    [db beginTransaction];
    BOOL result = [db executeUpdate:@"delete from Card where id = ?",card_Id];
    [db commit];
    return result;
}

-(BOOL)addCardIntoDataBase:(Card *)aCard {
    
    NSString *imageName = [aCard.imagePath lastPathComponent];
    NSString *soundName = [aCard.soundPath lastPathComponent];
    [db beginTransaction];
    BOOL result = [db executeUpdate:@"insert into Card (title,category,img_on,img_off,sound_path) values(?,?,?,?,?)",aCard.title,aCard.category,imageName,@"",soundName];
    [db commit];  
    return result;
}

-(BOOL)updateCard:(Card *)aCard {
    BOOL result = NO;
    [db beginTransaction];
    result = [db executeUpdate:@"update Card set title = ? where id = ?",aCard.title,aCard.cid];
    result = [db executeUpdate:@"update Card set category = ? where id = ?",aCard.category,aCard.cid];
    result = [db executeUpdate:@"update Card set img_on = ? where id = ?",[aCard.imagePath lastPathComponent],aCard.cid];
    result = [db executeUpdate:@"update Card set img_off = ? where id = ?",@"",aCard.cid];
    result = [db executeUpdate:@"update Card set sound_path = ? where id = ?",[aCard.soundPath lastPathComponent],aCard.cid];
    [db commit];  
    return result;
}

-(BOOL)addSentenceToDB:(Sentence *)aSentence {
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    NSMutableArray *idArray = [NSMutableArray arrayWithCapacity:[aSentence.cardArray count]];
    for (int i = 0; i < [aSentence.cardArray count]; i++) {
        Card *card = [aSentence.cardArray objectAtIndex:i];
        [idArray addObject:card.cid];
    }
    
    NSString *jsonStr = [idArray JSONString];
    NSNumber *playTimes = aSentence.playTimes;
    if (playTimes == nil) {
        playTimes = [NSNumber numberWithInt:0];
    }
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-d HH:mm:ss"];
    NSString *dateStr = [formatter stringFromDate:aSentence.createTime];
    [formatter release];
    
    NSString *titleStr = aSentence.title;
    [db beginTransaction];
    BOOL result = [db executeUpdate:@"insert into Sentence (sentence,title,createTime,playTimes) values(?,?,?,?)",jsonStr,titleStr,dateStr,playTimes];
    [db commit];    

    [pool drain];
    return result;
}

-(NSArray *)getAllSentence {
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    FMResultSet *rs = [db executeQuery:@"select * from Sentence"];
    while ([rs next]) {
        Sentence *sentence = [[Sentence alloc] init];
        int sid = [rs intForColumn:@"sid"];
        sentence.sid = [NSNumber numberWithInt:sid];
        
        NSString *cardIdJSONStr = [rs stringForColumn:@"sentence"];
        NSArray *idArray = [cardIdJSONStr objectFromJSONString];
        NSMutableArray *cardArray = [NSMutableArray arrayWithCapacity:[idArray count]];
        for (int i = 0; i < [idArray count]; i++) {
            [cardArray addObject:[self getCardDicWithCardId:[idArray objectAtIndex:i]]];
        }
        sentence.cardArray = [NSArray arrayWithArray:cardArray];
        
        NSString *sentenceTitle = [rs stringForColumn:@"title"];
        sentence.title = sentenceTitle;
        
        NSString *sentenceDateStr= [rs stringForColumn:@"createTime"];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-d HH:mm:ss"];
        sentence.createTime = [formatter dateFromString:sentenceDateStr];
        [formatter release];
        
        int playtimes = [rs intForColumn:@"playTimes"];
        sentence.playTimes = [NSNumber numberWithInt:playtimes];
        
        [tempArray addObject:sentence];
        [sentence release];
        
    }
    [pool drain];
    return [tempArray autorelease];
}

-(BOOL)deleteSentenceWithSid:(NSNumber *)sentence_Id {
    [db beginTransaction];
    BOOL result = [db executeUpdate:@"delete from Sentence where sid = ?",sentence_Id];
    [db commit];
    return result;
}


@end
