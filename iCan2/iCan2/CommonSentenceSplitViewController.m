//
//  CommonSentenceSplitViewController.m
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import "CommonSentenceSplitViewController.h"

@implementation CommonSentenceSplitViewController
@synthesize leftViewController,rightViewController;

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (leftViewController == nil) {
        leftViewController = [[SentenceTableViewController alloc] initWithStyle:UITableViewStylePlain];
        leftViewController.title = @"常用溝通語";
    }
    UINavigationController *lnav = [[UINavigationController alloc] initWithRootViewController:leftViewController];
    
    if (rightViewController == nil) {
        rightViewController = [[PlaySentenceViewController alloc] init];
        rightViewController.title = @"播放常用溝通語";
    }
    UINavigationController *rnav = [[UINavigationController alloc] initWithRootViewController:rightViewController];
    self.viewControllers = [NSArray arrayWithObjects:lnav,rnav, nil];
    

    [lnav release];
    [rnav release];
}

- (void)viewDidUnload
{
    self.rightViewController = nil;
    self.leftViewController = nil;
    [super viewDidUnload];
    
}

- (void)dealloc {
    self.rightViewController = nil;
    self.leftViewController = nil;
    [super dealloc];   
}

- (void)viewWillAppear:(BOOL)animated {
    [leftViewController reloadList];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        return YES;
    }
	return NO;
}


@end
