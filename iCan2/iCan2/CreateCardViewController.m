//
//  CreateCardViewController.m
//  iCan2
//
//  Created by 陳 雍協 on 11/10/6.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import "CreateCardViewController.h"
#import "DataBaseManager.h"

#define DOCUMENTS_FOLDER [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]

@interface UIImage (Tint)

- (UIImage *)tintedImageUsingColor:(UIColor *)tintColor;

@end

@implementation UIImage (Tint)

- (UIImage *)tintedImageUsingColor:(UIColor *)tintColor {
    UIGraphicsBeginImageContext(self.size);
    CGRect drawRect = CGRectMake(0, 0, self.size.width, self.size.height);
    [self drawInRect:drawRect];
    [tintColor set];
    UIRectFillUsingBlendMode(drawRect, kCGBlendModeSourceAtop);
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tintedImage;
}

@end

@implementation CreateCardViewController
@synthesize card,delegate;
@synthesize imageView,categoryBtn,nameField,playBtn,recordBtn,cancleBtn,finishBtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andCard:(Card *)aCard
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.card = aCard;
        isPlaying = NO;
        isRecording = NO;
        isPlayerAvailable = NO;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.card != nil) {
        [self setUpCard:card];
    }
    self.imageView.backgroundColor = kBackGroundColor;
    self.nameField.delegate = self;
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewDidTapped:)];
    [self.imageView addGestureRecognizer:gestureRecognizer];
    [gestureRecognizer release];
    
    [fileName release];
    fileName = [NSString stringWithFormat:@"file_%d",(int)[[NSDate date] timeIntervalSince1970]];
    [fileName retain];
    
}

- (void)viewDidUnload {
    self.imageView = nil;
    self.categoryBtn = nil;
    self.nameField = nil;
    self.playBtn = nil;
    self.recordBtn = nil;
    self.cancleBtn = nil;
    self.finishBtn = nil;
    [popOver release];
    popOver = nil;
    [super viewDidUnload];
}

- (void)dealloc {
    self.delegate = nil;
    self.card = nil;
    self.imageView = nil;
    self.categoryBtn = nil;
    self.nameField = nil;
    self.playBtn = nil;
    self.recordBtn = nil;
    self.cancleBtn = nil;
    self.finishBtn = nil;
    [newName release];
    [popOver release];
    [categoryField release];
    [captureImage release];
    [recorder release];
    [audioPlayer release];
    [fileName release];
    [recordSetting release];
    [recorderFilePath release];
    [recordData release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        return YES;
    }
	return NO;
}

#pragma mark record Control
- (void) startRecording {
    self.playBtn.enabled = NO;
    isRecording = YES;
    [self.recordBtn setImage:[UIImage imageNamed:@"stop_gray"] forState:UIControlStateNormal];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *err = nil;
    [audioSession setCategory :AVAudioSessionCategoryPlayAndRecord error:&err];
    if(err){
        NSLog(@"audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
    }
    [audioSession setActive:YES error:&err];
    err = nil;
    if(err){
        NSLog(@"audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
    }
    [recordSetting release];
    recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue :[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey]; 
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    [recordSetting setValue :[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
    [recordSetting setValue :[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
    [recordSetting setValue :[NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];
    
    // Create a new dated file
    NSString *caldate = [NSString stringWithFormat:@"%@.caf",fileName];
    [recorderFilePath release];
    recorderFilePath = [[NSString stringWithFormat:@"%@/%@", DOCUMENTS_FOLDER, caldate] retain];
    
    NSURL *url = [NSURL fileURLWithPath:recorderFilePath];
    err = nil;
    [recorder release];
    recorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&err];
    if(!recorder){
        NSLog(@"recorder: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: [err localizedDescription]
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [alert show];
        [alert release];
        return;
    }
    
    //prepare to record
    [recorder setDelegate:self];
    [recorder prepareToRecord];
    recorder.meteringEnabled = YES;
    
    BOOL audioHWAvailable = audioSession.inputIsAvailable;
    if (! audioHWAvailable) {
        UIAlertView *cantRecordAlert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: @"Audio input hardware not available"
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [cantRecordAlert show];
        [cantRecordAlert release]; 
        return;
    }
    
    // start recording
    [recorder recordForDuration:(NSTimeInterval) 10];
}

- (void) stopRecording {
    self.playBtn.enabled = YES;
    isRecording = NO;
    [self.recordBtn setImage:[UIImage imageNamed:@"record_gray"] forState:UIControlStateNormal];
    
    [recorder stop];
    
    NSURL *url = [NSURL fileURLWithPath: recorderFilePath];
    NSError *err = nil;
    NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:&err];
    if(!audioData) NSLog(@"audio data: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
    else {
        [recordData release];
        recordData = audioData;
        [recordData retain];
    }
}

- (void) playSound {
    
    isPlaying = YES;
    self.recordBtn.enabled = NO;
    [self.playBtn setImage:[UIImage imageNamed:@"stop_gray"] forState:UIControlStateNormal];
    
    NSData *soundData = nil;
    if (recordData) soundData = recordData;
    else soundData = card.soundData;
    
    NSError *error;   
    [audioPlayer release];
    audioPlayer = [[AVAudioPlayer alloc] initWithData:soundData error:&error] ;
    audioPlayer.delegate = self;   

    isPlayerAvailable = YES;
    if (!error) {
        [audioPlayer prepareToPlay];
        [audioPlayer setNumberOfLoops:0];
        [audioPlayer play];

    }
}



- (void) stopSound {
    if (isPlayerAvailable) {
        [self.playBtn setImage:[UIImage imageNamed:@"play_gray"] forState:UIControlStateNormal];
        self.recordBtn.enabled = YES;
        isPlaying = NO;
        [audioPlayer stop];
        [audioPlayer release];
        audioPlayer = nil;
        isPlayerAvailable = NO;
    }
}

#pragma mark - method
- (IBAction)categoryButtonDidClicked:(id)sender {
    NSArray *category = [[DataBaseManager defaultDataBaseManager] getAllCategory];
    int selectIndex = -1;
    for (int i = 0; i< [category count]; i++) {
        if ([card.category isEqualToString:[category objectAtIndex:i]]) {
            selectIndex = i;
            break;
        }
    }
    [popOver dismissPopoverAnimated:YES];
    [popOver release];
    CategorySelectionView *selectionView = [[CategorySelectionView alloc] initWithStyle:UITableViewStylePlain andCategory:category andSelectIndex:selectIndex];
    selectionView.delegate = self;
    popOver = [[UIPopoverController alloc] initWithContentViewController:selectionView];
    [selectionView release];
    [popOver presentPopoverFromRect:self.categoryBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

}

- (IBAction)playButtonDidClicked:(id)sender {
    if (isPlaying) [self stopSound];
    else [self playSound];
        
    
}

- (IBAction)recordButtonDidClicked:(id)sender {
    if (!isRecording) {
        [self startRecording];
    } else {
        [self stopRecording];
    } 
}

- (IBAction)cancleButtonDidClicked:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)finishButtonDidClicked:(id)sender {
    
//    NSData *imgData = UIImagePNGRepresentation(savedImage);
    //    [imgData writeToFile:[NSString stringWithFormat:@"%@/%@.png",DOCUMENTS_FOLDER,timeStamp] atomically:YES];
    //    updateImageName = [NSString stringWithFormat:@"%@.png",timeStamp];
    
    
    if (captureImage) {
        self.card.imagePath = [NSString stringWithFormat:@"%@.png",fileName];
        NSData *imgData = UIImagePNGRepresentation(captureImage);
        [imgData writeToFile:[NSString stringWithFormat:@"%@/%@.png",DOCUMENTS_FOLDER,fileName] atomically:YES];
        self.card.image = captureImage;
    }
    
    if (recordData) {
        self.card.soundPath = [NSString stringWithFormat:@"%@.caf",fileName];
        self.card.soundData = recordData;
    }
    
    if ([nameField.text length] > 0) {
        NSLog(@"finishButtonDidClicked");
        if ([nameField.text isEqualToString:self.card.title]) {
            if (![[DataBaseManager defaultDataBaseManager] updateCard:self.card]) {
                NSLog(@"更新失敗");
            }
        } else {
            self.card.title = nameField.text;
            if (![[DataBaseManager defaultDataBaseManager] addCardIntoDataBase:self.card]) {
                NSLog(@"新增失敗");
            }
        }
        
    }
    
    
    [self dismissModalViewControllerAnimated:YES];
}

- (void)setUpCard:(Card *)aCard {
    self.imageView.image = self.card.image;
    [self.imageView setHighlightedImage:[self.card.image tintedImageUsingColor:[UIColor colorWithWhite:0.0 alpha:0.3]]];
    [self.categoryBtn setTitle:self.card.category forState:UIControlStateNormal];
    if (newName) self.nameField.text = newName;
    else self.nameField.text = self.card.title;
    
    
}

- (void)imageViewDidTapped:(id)sender {
    UIActionSheet *actionSheet = nil;
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"選擇圖片來源"
                                                  delegate:self 
                                         cancelButtonTitle:@"取消"
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"從相簿選取",@"畫板", nil
                       ];
    } else {
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"選擇圖片來源"
                                                  delegate:self 
                                         cancelButtonTitle:@"取消"
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"拍照",@"從相簿選取",@"畫板", nil
                       ];
    }
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
	[actionSheet showInView:self.view]; 
	[actionSheet release];
    
    
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && buttonIndex == 0) {
        //iPad2 Camera
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentModalViewController:imagePicker animated:YES];
        [imagePicker release];
        
        
    } else if (([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && buttonIndex == 1) ||  
               (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && buttonIndex == 0)) {
        //Photo apbum
        [popOver dismissPopoverAnimated:YES];
        [popOver release];
        UIImagePickerController *photoAlbum = [[UIImagePickerController alloc] init];
        photoAlbum.delegate = self;
        photoAlbum.allowsEditing = YES;
        photoAlbum.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        popOver = [[UIPopoverController alloc] initWithContentViewController:photoAlbum];
        [photoAlbum release];
        [popOver presentPopoverFromRect:self.imageView.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else if (([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && buttonIndex == 2) ||  
               (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && buttonIndex == 1)) {
        //Draw view
        DrawViewController *drawView = [[DrawViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:drawView];
        drawView.title = @"畫板";
        drawView.delegate = self;
        [drawView release];
        
        [self presentModalViewController:nav animated:YES];
        [nav release];
        
        
    }
    
    
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [captureImage release];
    captureImage = [info objectForKey:UIImagePickerControllerEditedImage]; 
    [captureImage retain];
    self.card.image = captureImage;
    [picker dismissModalViewControllerAnimated:YES];
    [self setUpCard:card];
    [popOver dismissPopoverAnimated:YES];

}

#pragma mark - DrawViewControllerDelegate
- (void)drawViewController:(DrawViewController*)drawView getImage:(UIImage *)image {
    [captureImage release];
    captureImage = image;
    [captureImage retain];
    self.card.image = captureImage;
    [self setUpCard:card];
}


#pragma mark - CategorySelectionViewDelegate
- (void)categorySelectionView:(CategorySelectionView *)vc didSeleteCategory:(NSString *)categoryName {
    self.card.category = categoryName;
    [self setUpCard:self.card];
    [popOver dismissPopoverAnimated:YES];
}

- (void)categorySelectionViewDidSeleteAddCategory:(CategorySelectionView *)vc {
    [categoryField release];
    categoryField = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 30.0)];
    categoryField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    categoryField.borderStyle = UITextBorderStyleRoundedRect;
    categoryField.keyboardType = UIKeyboardTypeNumberPad;
    categoryField.returnKeyType = UIReturnKeyDone;
    categoryField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    categoryField.placeholder = @"類別名稱";
    [categoryField becomeFirstResponder];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"請輸入類別名稱"
                                                    message:@"類別名稱" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"好", nil];
    [alert addSubview:categoryField];
    [alert show];
    [alert release];
    [popOver dismissPopoverAnimated:YES];
    
}

#pragma mark -
#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        if ([categoryField.text length]>0) {
            self.card.category = categoryField.text;
            [self setUpCard:self.card];
        }        
    }
}

#pragma mark -
#pragma mark AVAudioRecorderDelegate
- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *) aRecorder successfully:(BOOL)flag {
    NSLog(@"audioRecorderDidFinishRecording:successfully:");
    // your actions here
    
}

#pragma mark -
#pragma mark AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    [self.playBtn setImage:[UIImage imageNamed:@"play_gray"] forState:UIControlStateNormal];
    self.recordBtn.enabled = YES;
    [audioPlayer release];
    audioPlayer = nil;
    isPlayerAvailable = NO;
    isPlaying = NO;
    
}

#pragma mark -
#pragma mark UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [newName release];
    newName = textField.text;
    [newName retain];
}

@end
