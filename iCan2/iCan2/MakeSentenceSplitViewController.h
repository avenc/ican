//
//  MakeSentenceView.h
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryTableViewContrller.h"
#import "MakeSentenceViewController.h"
#import "DataBaseManager.h"
@interface MakeSentenceSplitViewController : UISplitViewController {
    CategoryTableViewContrller *leftViewController;
    MakeSentenceViewController *rightViewController;
}
@property (nonatomic, retain) CategoryTableViewContrller *leftViewController;
@property (nonatomic, retain) MakeSentenceViewController *rightViewController;

@end
