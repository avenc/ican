//
//  CardView.m
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import "CardView.h"
#define DRAG_THRESHOLD 5


float distanceBetweenPoints(CGPoint a, CGPoint b);

@implementation CardImageView
@synthesize delegate;
@synthesize home;
@synthesize touchLocation;
@synthesize inTarget;
@synthesize index;
@synthesize card;

- (id)initWithImage:(UIImage *)image {
    self = [super initWithImage:image];
    if (self) {
        [self setUserInteractionEnabled:YES];
        [self setExclusiveTouch:YES];  // block other touches while dragging a thumb view
        inTarget = NO;
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // store the location of the starting touch so we can decide when we've moved far enough to drag
    touchLocation = [[touches anyObject] locationInView:self];
    if ([delegate respondsToSelector:@selector(thumbImageViewStartedTracking:)])
        [delegate thumbImageViewStartedTracking:self];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    // we want to establish a minimum distance that the touch has to move before it counts as dragging,
    // so that the slight movement involved in a tap doesn't cause the frame to move.
    
    CGPoint newTouchLocation = [[touches anyObject] locationInView:self];
    
    // if we're already dragging, move our frame
    if (dragging) {
        float deltaX = newTouchLocation.x - touchLocation.x;
        float deltaY = newTouchLocation.y - touchLocation.y;
        [self moveByOffset:CGPointMake(deltaX, deltaY)];
    }
    
    // if we're not dragging yet, check if we've moved far enough from the initial point to start
    else if (distanceBetweenPoints(touchLocation, newTouchLocation) > DRAG_THRESHOLD) {
        touchLocation = newTouchLocation;
        dragging = YES;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (dragging) {
        if (inTarget) {
            [self gohomeWithNoAnimation];
        }else {
            [self goHome];
        }
        dragging = NO;
    } else if ([[touches anyObject] tapCount] == 1) {
        if ([delegate respondsToSelector:@selector(thumbImageViewWasTapped:)])
            [delegate thumbImageViewWasTapped:self];
    }
    
    if ([delegate respondsToSelector:@selector(thumbImageViewStoppedTracking:)]) 
        [delegate thumbImageViewStoppedTracking:self];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [self goHome];
    dragging = NO;
    if ([delegate respondsToSelector:@selector(thumbImageViewStoppedTracking:)]) 
        [delegate thumbImageViewStoppedTracking:self];
}

- (void)goHome {
    // distance is in pixels
    float distanceFromHome = distanceBetweenPoints([self frame].origin, [self home].origin);  
    // duration is in seconds, so each additional pixel adds only 1/1000th of a second.
    float animationDuration = 0.1 + distanceFromHome * 0.001; 
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    [self setFrame:[self home]];
    [UIView commitAnimations];
}


- (void)gohomeWithNoAnimation{
    self.alpha = 0;
    [self setFrame:[self home]];
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1;
    }];
}


- (void)moveByOffset:(CGPoint)offset {
    CGRect frame = [self frame];
    frame.origin.x += offset.x;
    frame.origin.y += offset.y;
    [self setFrame:frame];
    if ([delegate respondsToSelector:@selector(thumbImageViewMoved:)]){
        [delegate thumbImageViewMoved:self];
    }
    
}    

- (void)dealloc {
    [card release];
    [super dealloc];
}

@end


float distanceBetweenPoints(CGPoint a, CGPoint b) {
    float deltaX = a.x - b.x;
    float deltaY = a.y - b.y;
    return sqrtf( (deltaX * deltaX) + (deltaY * deltaY) );
}


@implementation CardView
@synthesize imageView;
@synthesize placeholderImageView;
@synthesize label;

- (id)initWithFrame:(CGRect)frame andCard:(Card *)aCard {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        CGRect tempFrame = frame;
        tempFrame.size.width = 141.0;
        tempFrame.size.height = 200.0;
        self.frame = tempFrame;
        
        imageView = [[CardImageView alloc] initWithImage:aCard.image];
        imageView.frame = CGRectMake(5, 10, 130, 130);
        [imageView setHome:imageView.frame];
        imageView.card = aCard;
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [imageView.layer setMasksToBounds:YES];
            [imageView.layer setBorderWidth:1.0];
            [imageView.layer setBorderColor:[[UIColor grayColor] CGColor]];
            [imageView.layer setCornerRadius:5.0];
        }
        
        
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(5, 135, 141, 50)];
        label.opaque = NO;
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont boldSystemFontOfSize:18.0];
        label.textAlignment = UITextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.text = aCard.title;
        
        placeholderImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, 130, 130)];
        placeholderImageView.image = imageView.image;
        placeholderImageView.alpha = 0.5;
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [placeholderImageView.layer setMasksToBounds:YES];
            [placeholderImageView.layer setBorderWidth:1.0];
            [placeholderImageView.layer setBorderColor:[[UIColor grayColor] CGColor]];
            [placeholderImageView.layer setCornerRadius:5.0];
        }
        
        [self addSubview:placeholderImageView];
        [self addSubview:imageView];
        [self addSubview:label];
        [self sendSubviewToBack:label];
    }
    return self;

}

- (void)dealloc
{
    [placeholderImageView release];
    [imageView release];
    [label release];
    [super dealloc];
}



@end
