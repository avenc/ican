//
//  PlaySentenceViewController.h
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import "DataBaseManager.h"
#import "Sentence.h"
#import "SentenceLabelCard.h"
@interface PlaySentenceViewController : UIViewController <AVAudioPlayerDelegate>{
    UIImageView *mainImage;
    UILabel *mainTitle;
    UIScrollView *scrollView;
//    NSArray *cardArray;
    Sentence *sentence;
    
    //player control    
    AVAudioPlayer *audioPlayer;
    BOOL pause;
    BOOL isPlayerAvailable;
    UIButton *playBtn;
    UIButton *pauseBtn;
    UIButton *stopBtn; 
    int speechIndex;
}
@property(nonatomic,retain) Sentence *sentence;
-(void)reloadDataWithSentence:(Sentence *)aSentence;

-(void)playClicked:(id)sender;
-(void)pauseClicked:(id)sender;
-(void)stopClicked:(id)sender;

-(void)playAtIndex:(int)index;

@end
