//
//  PlaySentenceViewController.m
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import "PlaySentenceViewController.h"

@implementation PlaySentenceViewController
@synthesize sentence;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = kBackGroundColor;
    
    //player state control
    pause = NO;
    isPlayerAvailable = NO;
    playBtn.enabled = YES;
    pauseBtn.enabled = NO;     
    mainImage = [[UIImageView alloc] initWithFrame:CGRectMake(50, 50, 250, 250)];
    mainImage.backgroundColor = [UIColor darkGrayColor];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [mainImage.layer setMasksToBounds:YES];
        [mainImage.layer setBorderWidth:1.0];
        [mainImage.layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
        [mainImage.layer setCornerRadius:5.0];
    }
    
    
    [self.view addSubview:mainImage];
    [mainImage release];
    
    mainTitle = [[UILabel alloc] initWithFrame:CGRectMake(mainImage.frame.origin.x, mainImage.frame.origin.y+mainImage.frame.size.height+15, 250, 25)];
    mainTitle.backgroundColor = [UIColor clearColor];
    mainTitle.textAlignment = UITextAlignmentCenter;
    mainTitle.font = [UIFont boldSystemFontOfSize:25];
    mainTitle.textColor = [UIColor blackColor];
    [self.view addSubview:mainTitle];
    [mainTitle release];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, mainTitle.frame.origin.y+mainTitle.frame.size.height+90, 705, 250)];
    scrollView.backgroundColor = [UIColor grayColor];
    [self.view addSubview:scrollView];
    [scrollView release];
    
    
    playBtn = [[UIButton alloc] initWithFrame:CGRectMake(mainImage.frame.origin.x + mainImage.frame.size.width + 80, (mainImage.frame.size.height/2)+mainImage.frame.origin.y , 60, 60)];
    [playBtn addTarget:self action:@selector(playClicked:) forControlEvents:UIControlEventTouchUpInside];
    [playBtn setImage:[UIImage imageNamed:@"play_gray"] forState:UIControlStateNormal];
    [self.view addSubview:playBtn];
    [playBtn release];
    
    pauseBtn = [[UIButton alloc] initWithFrame:CGRectMake(playBtn.frame.size.width + playBtn.frame.origin.x +10, playBtn.frame.origin.y , 60, 60)];
    [pauseBtn addTarget:self action:@selector(pauseClicked:) forControlEvents:UIControlEventTouchUpInside];
    [pauseBtn setImage:[UIImage imageNamed:@"pause_gray"] forState:UIControlStateNormal];
    [self.view addSubview:pauseBtn];
    [pauseBtn release];
    
    stopBtn = [[UIButton alloc] initWithFrame:CGRectMake(pauseBtn.frame.size.width + pauseBtn.frame.origin.x +10, playBtn.frame.origin.y , 60, 60)];
    [stopBtn addTarget:self action:@selector(stopClicked:) forControlEvents:UIControlEventTouchUpInside];
    [stopBtn setImage:[UIImage imageNamed:@"stop_gray"] forState:UIControlStateNormal];
    [self.view addSubview:stopBtn];
    [stopBtn release];
    
    NSArray *sentenceArray = [[DataBaseManager defaultDataBaseManager] getAllSentence];
    if (sentenceArray!=nil && [sentenceArray count]!=0) 
        [self reloadDataWithSentence:[sentenceArray objectAtIndex:0]];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [sentence release];
    [super dealloc];
}
    
-(void)reloadDataWithSentence:(Sentence *)aSentence {
    self.title = aSentence.title;
    self.sentence = aSentence;
    
    for (UIView *view in scrollView.subviews) {
        [view removeFromSuperview];
    }
    Card *firstCard = (Card*)[sentence.cardArray objectAtIndex:0];
    mainImage.image = firstCard.image;
    scrollView.contentSize = CGSizeMake(200*[sentence.cardArray count]+40*([sentence.cardArray count]-1)+10, scrollView.frame.size.height);
    mainTitle.text = firstCard.title; 
    
    int orginx = 10;
    for (int i = 0; i<[sentence.cardArray count]; i++) {
        SentenceLabelCard *card = [[SentenceLabelCard alloc] initWithCard:[sentence.cardArray objectAtIndex:i] andIndex:i];
        
        CGRect frame = card.frame;
        frame.origin.x = orginx;
        orginx = orginx + 240;
        card.frame = frame;
        
        
        [scrollView addSubview:card];
        [card release];
        
        if (i != 0) {
            UIImageView *arrowhead = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrowhead"]];
            arrowhead.frame = CGRectMake(card.frame.origin.x+ 10 - 40, card.frame.size.height/2, 15, 15);
            
            [scrollView addSubview:arrowhead];
            [arrowhead release];
        }
        
    }
    
    [scrollView scrollRectToVisible:CGRectMake(0, 0, scrollView.frame.size.width, scrollView.frame.size.height) animated:NO];
}


-(void)playClicked:(id)sender {
    playBtn.enabled = NO;
    pauseBtn.enabled = YES; 
    if (pause) {
        [audioPlayer play];
        return;
    } else {
        
        speechIndex = 0;
        [self playAtIndex:speechIndex];
    }
}

-(void)pauseClicked:(id)sender {
    playBtn.enabled = YES;
    pauseBtn.enabled = NO;    
    if (isPlayerAvailable) {
        pause = YES;
        [audioPlayer pause];
    }
}

-(void)stopClicked:(id)sender {
    if (isPlayerAvailable) {
        pause = NO;
        [audioPlayer stop];
        [audioPlayer release];
        isPlayerAvailable = NO;
        playBtn.enabled = YES;
        pauseBtn.enabled = NO;
        speechIndex = 0;
        
        Card *card = [sentence.cardArray objectAtIndex:speechIndex];
        //        mainImage.image = [UIImage imageNamed:[dic objectForKey:@"img_on"]];
        mainImage.image = card.image;
        mainTitle.text = card.title;
        [scrollView scrollRectToVisible:CGRectMake(0, 0, scrollView.frame.size.width, scrollView.frame.size.height) animated:YES];
    }
}

-(void)playAtIndex:(int)index {
    if (index>=[sentence.cardArray count]) return;
    
    Card *card = [sentence.cardArray objectAtIndex:index];
    NSData *sound_data = card.soundData;
    
    NSError *error;   
    audioPlayer = [[AVAudioPlayer alloc] initWithData:sound_data error:&error] ;
    audioPlayer.delegate = self;   
    
    isPlayerAvailable = YES;
    if (!error) {
        [audioPlayer prepareToPlay];
        [audioPlayer setNumberOfLoops:0];
        [audioPlayer play];
        
        //        mainImage.image = [UIImage imageNamed:[dic objectForKey:@"img_on"]];
        mainImage.image = card.image;
        mainTitle.text = card.title;
        [scrollView scrollRectToVisible:CGRectMake(240*index, 0, scrollView.frame.size.width, scrollView.frame.size.height) animated:YES];
    }
}

#pragma mark -
#pragma mark AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    [audioPlayer release];
    isPlayerAvailable = NO;
    if (speechIndex<[sentence.cardArray count]){
        speechIndex = speechIndex+1;
        [self playAtIndex:speechIndex];
    }
    
    if (speechIndex == [sentence.cardArray count]) {
        speechIndex = 0;
        pause = NO;
        playBtn.enabled = YES;
        pauseBtn.enabled = NO;  
        Card *card = [sentence.cardArray objectAtIndex:speechIndex];
        mainImage.image = card.image;
        mainTitle.text = card.title;
        [scrollView scrollRectToVisible:CGRectMake(0, 0, scrollView.frame.size.width, scrollView.frame.size.height) animated:YES];
    }
    
}


@end
