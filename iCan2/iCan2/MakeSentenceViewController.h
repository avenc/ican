//
//  AsembleSentenceViewController.h
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "CardView.h"
#import "DataBaseManager.h"
#import "SentenceCardView.h"

@interface SentenceView : UIScrollView {
    UIScrollView *scrollView;    
}
@property (nonatomic, retain) UIScrollView *scrollView;
@end

@interface MakeSentenceViewController : UIViewController <UIScrollViewDelegate,CardImageViewDelegate,AVAudioPlayerDelegate> {
    UIScrollView *scrollView;
    UIPageControl *pageControl;
    NSArray *cards;
    BOOL pageControlUsed;
    int pages;
    int lastCardIndex;
    NSMutableArray *speechArray;
    BOOL isInTarget;
    int speechIndex;
    SentenceView *sentenceView;
}
@property (nonatomic, retain) UIScrollView *scrollView;
@property (nonatomic, retain) UIPageControl *pageControl;
@property (nonatomic, retain) NSArray *cards;
@property (nonatomic, assign) int lastCardIndex;
@property (nonatomic, retain) NSMutableArray *speechArray;
@property (nonatomic, retain) SentenceView *sentenceView;
- (void)changePage:(id)sender;
- (void)reloadDataWithCardArray:(NSArray*)array;
- (CGRect)getRectAtCardIndex:(int)index;
-(BOOL)isCardInSentenceView:(CardImageView *)cardImage;
-(void)playSoundWithCard:(Card *)aCard;
-(void)playButtonPressed:(id)sender;
-(void)clearButtonPressed:(id)sender;
-(void)saveButtonPressed:(id)sender;
@end


