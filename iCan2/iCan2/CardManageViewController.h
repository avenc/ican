//
//  CardManageViewController.h
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardButton.h"
#import "DataBaseManager.h"

@interface CardManageViewController : UIViewController<UIScrollViewDelegate,CardButtonDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate> {
    UIScrollView *scrollView;
    UIPageControl *pageControl;
    BOOL pageControlUsed;
    int pages;
    UIPopoverController *popover ;
    NSString *category;
}
@property (nonatomic, retain) UIScrollView *scrollView;
@property (nonatomic, retain) UIPageControl *pageControl;
@property (nonatomic, retain) NSString *category;
- (void)changePage:(id)sender;
- (void)reloadDataWithCardArray:(NSArray*)array;

@end
