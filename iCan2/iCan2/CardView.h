//
//  CardView.h
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Card.h"
#import <QuartzCore/QuartzCore.h>

@protocol CardImageViewDelegate;

@interface CardImageView : UIImageView {
    id <CardImageViewDelegate> delegate;
    CGRect home;
    BOOL dragging;
    CGPoint touchLocation; // Location of touch in own coordinates (stays constant during dragging).
    BOOL inTarget;
    int index;
    Card *card;
}
@property (nonatomic, assign) id <CardImageViewDelegate> delegate;
@property (nonatomic, assign) CGRect home;
@property (nonatomic, assign) CGPoint touchLocation;
@property (nonatomic) BOOL inTarget;
@property (nonatomic, assign) int index;
@property (nonatomic, retain) Card *card;
- (void)goHome;  // animates return to home location
- (void)gohomeWithNoAnimation;
- (void)moveByOffset:(CGPoint)offset; // change frame lo

@end

@protocol CardImageViewDelegate <NSObject>

@optional
- (void)thumbImageViewWasTapped:(CardImageView *)tiv;
- (void)thumbImageViewStartedTracking:(CardImageView *)tiv;
- (void)thumbImageViewMoved:(CardImageView *)tiv;
- (void)thumbImageViewStoppedTracking:(CardImageView *)tiv;

@end

@interface CardView : UIView {
    CardImageView *imageView;
    UIImageView *placeholderImageView;
    UILabel *label;
}
@property(nonatomic,retain)CardImageView *imageView;
@property(nonatomic,retain)UIImageView *placeholderImageView;
@property(nonatomic,retain)UILabel *label;

- (id)initWithFrame:(CGRect)frame andCard:(Card *)aCard;
@end