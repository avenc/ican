//
//  AsembleSentenceViewController.m
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import "MakeSentenceViewController.h"
#import "Sentence.h"
#define Sentence_card_count 10



@implementation SentenceView
@synthesize scrollView;

- (id)init {
    self = [super initWithFrame:CGRectMake(0, 520, 705, 160)];
    if (self) {
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blankbackground.png"]];
        self.contentSize = CGSizeMake(150*Sentence_card_count , 160);
    }
    return self;
}


- (void)dealloc {
    [scrollView release];
    [super dealloc];
}

@end



@implementation MakeSentenceViewController
@synthesize scrollView;
@synthesize pageControl;
@synthesize cards;
@synthesize lastCardIndex;
@synthesize speechArray;
@synthesize sentenceView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 705, 768-44-44)];
    backgroundView.backgroundColor = kBackGroundColor;
    self.view = backgroundView;
    [backgroundView release];
    
    if (scrollView == nil) {
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(-1, 0, 705, 400)];
        scrollView.backgroundColor = kBackGroundColor;
        scrollView.pagingEnabled = YES;
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.scrollsToTop = NO;
        scrollView.delegate = self;
        [scrollView setClipsToBounds:NO];
        [scrollView setCanCancelContentTouches:NO];
    }
    
    if (pageControl == nil) {
        pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.scrollView.frame.size.height + 10.0, 705, 20.0)];
        pageControl.backgroundColor = [UIColor clearColor];
        pageControl.currentPage = 0;
        [pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
    }

    if (sentenceView == nil) {
        sentenceView = [[SentenceView alloc] init];
        CGRect frame = CGRectMake(0, 0, 150, 160);
        for (int i = 0; i<Sentence_card_count; i++) {
            SentenceCardView *cardView = [[SentenceCardView alloc] initWithFrame:frame];
            cardView.index = i;
            [sentenceView addSubview:cardView];
            [cardView release];
            frame.origin.x = frame.origin.x +150;
        }
    }
    
    UIButton *playButton = [[UIButton alloc] initWithFrame:CGRectMake(100, self.scrollView.frame.size.height + 40.0, 160, 40)];
    
    [playButton setBackgroundImage:[UIImage imageNamed:@"btn_gray_off"] forState:UIControlStateNormal];
    [playButton addTarget:self action:@selector(playButtonPressed:) 
         forControlEvents:UIControlEventTouchUpInside];
    [playButton setTitle:@"播放" forState:UIControlStateNormal];
    [playButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    UIButton *clearButton = [[UIButton alloc] initWithFrame:CGRectMake(playButton.frame.origin.x+playButton.frame.size.width+5, self.scrollView.frame.size.height + 40.0, 160, 40)];
    [clearButton setBackgroundImage:[UIImage imageNamed:@"btn_gray_off"] forState:UIControlStateNormal];
    [clearButton addTarget:self action:@selector(clearButtonPressed:) 
          forControlEvents:UIControlEventTouchUpInside];
    [clearButton setTitle:@"清除" forState:UIControlStateNormal]; 
    [clearButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    UIButton *saveButton = [[UIButton alloc] initWithFrame:CGRectMake(clearButton.frame.origin.x+playButton.frame.size.width +5, self.scrollView.frame.size.height + 40.0,160, 40)];
    [saveButton setBackgroundImage:[UIImage imageNamed:@"btn_gray_off"] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveButtonPressed:) 
         forControlEvents:UIControlEventTouchUpInside];
    [saveButton setTitle:@"儲存" forState:UIControlStateNormal];
    [saveButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:playButton];
    [playButton release];
    [self.view addSubview:clearButton];
    [clearButton release];
    [self.view addSubview:saveButton];
    [saveButton release];
    
    [self.view addSubview:scrollView];
    [self.view addSubview:pageControl];
    [self.view addSubview:sentenceView];
    
    lastCardIndex = 0;
    self.speechArray = [NSMutableArray arrayWithCapacity:Sentence_card_count];
    [self reloadDataWithCardArray:[[DataBaseManager defaultDataBaseManager]getCardWithCategory:@"交通工具"]];
    isInTarget = NO;
    speechIndex = -1;
}


- (void)viewDidUnload
{
    self.sentenceView = nil;
    self.pageControl = nil;
    self.scrollView = nil;
    [super viewDidUnload];
}

- (void)dealloc
{
    [sentenceView release];
    [speechArray release];
    [scrollView release];
    [pageControl release];
    [cards release];
    [super dealloc];
}

#pragma mark -
#pragma mark UIScrollViewDelegate

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlUsed = NO;
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (pageControlUsed) return;
	
    // Switch the indicator when more than 50% of the previous/next page is visible
	CGFloat pageWidth = scrollView.frame.size.width;
	int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = page;
}

#pragma mark - 
#pragma mark - method
- (void)loadScrollViewWithPage:(int)page {
    if (page < 0 || page >= pages)
        return;
    
}

- (CGRect)getRectAtCardIndex:(int)index{
    if (index%2 == 0) {
        int rowIndex = index/2;
        return CGRectMake(141*rowIndex, 0, 140, 200);
        
    }else{
        int rowIndex = index/2;
        return CGRectMake(141*rowIndex, 210, 140, 200);
        
    }
    return CGRectZero;
}

-(BOOL)isCardInSentenceView:(CardImageView *)cardImage {
    if (cardImage.index%2==1) {
        if (cardImage.center.y>330) return YES;
    } else if(cardImage.index%2==0){
        if (cardImage.center.y>530) return YES;
    }    
    return NO;
}

- (void)changePage:(id)sender {
    int page = pageControl.currentPage;
    CGRect frame = scrollView.frame;
    frame.origin.x = 705 * page;
    frame.origin.y = 0;
    [scrollView scrollRectToVisible:frame animated:YES];
    pageControlUsed = YES;
}

- (void)reloadDataWithCardArray:(NSArray*)array {
    self.scrollView.alpha = 0;
    self.cards = array;
    if ([cards count]%10 == 0) pages = [cards count]/10;
    else pages = (int)([cards count]/10)+1;
    
    [scrollView scrollRectToVisible:CGRectMake(-1, 0, 705, 400) animated:NO];    
    scrollView.contentSize = CGSizeMake(705*pages+1, 400);
    pageControl.numberOfPages = pages;
    for (UIView *view in scrollView.subviews) {
        [view removeFromSuperview];        
    }
    
    //Add CardView into scrollview
    for (int i = 0; i<[cards count]; i++) {
        CardView *cardView = [[CardView alloc] initWithFrame:[self getRectAtCardIndex:i] andCard:[cards objectAtIndex:i]];
        cardView.imageView.delegate = self;  
        cardView.imageView.index = i;
        [scrollView addSubview:cardView];
        [cardView release];
    }
    [UIView animateWithDuration:0.3 animations:^{
        self.scrollView.alpha = 1;
    }];
    
}

-(void)playButtonPressed:(id)sender {
    if ([speechArray count]==0) return;
    
    speechIndex = 0;
    Card *card = [speechArray objectAtIndex:speechIndex];
    speechIndex = speechIndex+1;
    [self playSoundWithCard:card];
}

-(void)playSoundWithCard:(Card *)aCard{

    NSData *sound_data = aCard.soundData;
    
    NSError *error;   
    AVAudioPlayer *audioPlayer = [[AVAudioPlayer alloc] initWithData:sound_data error:&error] ;
    audioPlayer.delegate = self;   
    
    if (!error) {
        [audioPlayer prepareToPlay];
        [audioPlayer setNumberOfLoops:0];
        [audioPlayer play];
    }
}

-(void)clearButtonPressed:(id)sender {
    if ([speechArray count]==0) return;

    for (UIView *view in sentenceView.subviews) {
        [view removeFromSuperview];
    }
    CGRect frame = CGRectMake(0, 0, 150, 160);    
    for (int i = 0; i<Sentence_card_count; i++) {
        SentenceCardView *cardView = [[SentenceCardView alloc] initWithFrame:frame];
        cardView.index = i;
        [sentenceView addSubview:cardView];
        [cardView release];
        frame.origin.x = frame.origin.x +150;
    }
    lastCardIndex = 0;
    speechIndex = -1;
    [speechArray removeAllObjects];
}

-(void)saveButtonPressed:(id)sender {
    if ([speechArray count]==0) return;
    
    Sentence *sentence = [[Sentence alloc] initWithCards:[NSArray arrayWithArray:speechArray]];
    
    if ([[DataBaseManager defaultDataBaseManager] addSentenceToDB:sentence]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"成功" message:[NSString stringWithFormat:@"「%@」\n成功加入常用句子",sentence.title] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"失敗" message:[NSString stringWithFormat:@"「%@」\n無法加入常用句子",sentence.title] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    [sentence release];
    
}

#pragma mark -
#pragma mark CardImageViewDelegate
- (void)thumbImageViewWasTapped:(CardImageView *)tiv{
    NSError *error;   
    AVAudioPlayer *audioPlayer = [[AVAudioPlayer alloc] initWithData:tiv.card.soundData error:&error] ;
    audioPlayer.delegate = self;    
    
    if (!error) {
        [audioPlayer prepareToPlay];
        [audioPlayer setNumberOfLoops:0];
        [audioPlayer play];
    }
    speechIndex = -1;
    
}
- (void)thumbImageViewStartedTracking:(CardImageView *)tiv{
    UIView *tmpView = tiv;
    while ([tmpView superview]!=nil) {
        [[tmpView superview] bringSubviewToFront:tmpView];
        tmpView = [tmpView superview];
    }
    //reach the root view    
    for (UIView *view in tmpView.subviews){
        [tmpView bringSubviewToFront:view];
    }
    
}
- (void)thumbImageViewMoved:(CardImageView *)tiv{
    [self.view bringSubviewToFront:tiv];
    scrollView.scrollEnabled = NO;
    
    if ([self isCardInSentenceView:tiv]) {
        if (lastCardIndex<Sentence_card_count) {
            SentenceCardView *card = (SentenceCardView*)[[sentenceView subviews] objectAtIndex:lastCardIndex];
            card.imageView.backgroundColor = [UIColor darkGrayColor];
            isInTarget = YES;
            tiv.inTarget = YES;
            float regionX = 150*lastCardIndex-705/2;
            if (regionX<0) {
                regionX = 0;
            }
            
            [sentenceView scrollRectToVisible:CGRectMake(regionX, 0, 705, 160) animated:YES];
        }
        
    } else {
        if (lastCardIndex<Sentence_card_count) {
            SentenceCardView *card = (SentenceCardView*)[[sentenceView subviews] objectAtIndex:lastCardIndex];
            card.imageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BlankGrid"]];
            isInTarget = NO;
            tiv.inTarget = NO;
        }
        
    }
    
    
}
- (void)thumbImageViewStoppedTracking:(CardImageView *)tiv{
    
    scrollView.scrollEnabled = YES;
    if (isInTarget) {
        if (lastCardIndex<Sentence_card_count) {
            SentenceCardView *cardView = (SentenceCardView*)[[sentenceView subviews] objectAtIndex:lastCardIndex];
            UIImage *image = [tiv.image copy];
            cardView.imageView.image = image;
            [image release];
            cardView.card = tiv.card;
            [cardView setUpCard:tiv.card];
            lastCardIndex++; 
            [speechArray addObject:cardView.card];
        }
    } else {
        if (lastCardIndex<Sentence_card_count) {
            SentenceCardView *cardView = (SentenceCardView*)[[sentenceView subviews] objectAtIndex:lastCardIndex];
            cardView.imageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BlankGrid"]];
            cardView.card = nil;
            cardView.label.text = @"";
        }
        
    }
    
    isInTarget = NO;
    tiv.inTarget = NO;
}

#pragma mark -
#pragma mark AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    //    NSLog(@"%@",player);
    [player release];
    if (speechIndex == -1) {
        return;
    } else if (speechIndex<[speechArray count]){
        [self playSoundWithCard:[speechArray objectAtIndex:speechIndex]];
        speechIndex = speechIndex+1;
    }
}

@end
