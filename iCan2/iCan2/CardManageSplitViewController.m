//
//  CardManageSplitViewController.m
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import "CardManageSplitViewController.h"

@implementation CardManageSplitViewController
@synthesize leftViewController,rightViewController;


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [leftViewController refresh];
    [rightViewController reloadDataWithCardArray:[[DataBaseManager defaultDataBaseManager] getCardWithCategory:[leftViewController.categoryList objectAtIndex:0]]];
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (leftViewController == nil) {
        leftViewController = [[CradManageTableViewController alloc] initWithStyle:UITableViewStylePlain];
        leftViewController.title = @"選擇類別";
    }
    UINavigationController *lnav = [[UINavigationController alloc] initWithRootViewController:leftViewController];
    
    if (rightViewController == nil) {
        rightViewController = [[CardManageViewController alloc] init];
    }
    UINavigationController *rnav = [[UINavigationController alloc] initWithRootViewController:rightViewController];
    self.viewControllers  = [NSArray arrayWithObjects:lnav,rnav, nil];
    [lnav release];
    [rnav release];
}

- (void)viewDidUnload
{
    self.rightViewController = nil;
    self.leftViewController = nil;
    [super viewDidUnload];
    
}

- (void)dealloc {
    self.rightViewController = nil;
    self.leftViewController = nil;
    [super dealloc];   
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        return YES;
    }
	return NO;
}


@end
