//
//  iCan2AppDelegate.m
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import "iCan2AppDelegate.h"
#import "CardManageSplitViewController.h"
#import "CommonSentenceSplitViewController.h"
#import "MakeSentenceSplitViewController.h"
#import "DataBaseManager.h"

@implementation iCan2AppDelegate

@synthesize window = _window;
@synthesize tabBarController;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    NSString *tempDBPathInAppBundle = [[NSBundle mainBundle] pathForResource:@"db" ofType:@"sqlite"];
	NSString *tempDBPathInAppDocuments = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"db.sqlite"];    
    if (![[NSFileManager defaultManager] fileExistsAtPath:tempDBPathInAppDocuments]) {
        [[NSFileManager defaultManager] copyItemAtPath:tempDBPathInAppBundle toPath:tempDBPathInAppDocuments error:nil];
    }
    
    MakeSentenceSplitViewController *firstVC = [[MakeSentenceSplitViewController alloc] init];
    firstVC.title = @"溝通圖卡與造句";
    firstVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:firstVC.title 
                                                    image:[UIImage imageNamed:@"MakeSentence.png"]
                                                      tag:0];
    
    CommonSentenceSplitViewController *secondVC = [[CommonSentenceSplitViewController alloc] init];
    secondVC.title = @"常用句子";
    secondVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:secondVC.title 
                                                     image:[UIImage imageNamed:@"FreqUseSentence.png"]
                                                       tag:1];
    
    CardManageSplitViewController *thirdVC = [[CardManageSplitViewController alloc] init];
    thirdVC.title = @"新增圖卡與編輯";
    thirdVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:thirdVC.title 
                                                     image:[UIImage imageNamed:@"CreateNewCard.png"]
                                                       tag:2];
    
    
    tabBarController = [[UITabBarController alloc] init];
    tabBarController.viewControllers = [NSArray arrayWithObjects:firstVC, secondVC, thirdVC, nil];
    
    [firstVC release];
    [secondVC release];
    [thirdVC release];
    
    self.window.rootViewController = tabBarController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [DataBaseManager releaseDataBaseManager];
}

- (void)dealloc
{
    [tabBarController release];
    [_window release];
    [super dealloc];
}

@end
