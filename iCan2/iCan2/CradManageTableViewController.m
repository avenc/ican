//
//  CradManageTableViewController.m
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import "CradManageTableViewController.h"
#import "LabelCell.h"
#import "DataBaseManager.h"
#import "CardManageViewController.h"
@implementation CradManageTableViewController
@synthesize categoryList;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = kBackGroundColor;
    self.clearsSelectionOnViewWillAppear = NO;
    self.categoryList = [[DataBaseManager defaultDataBaseManager] getAllCategory];
}

- (void)dealloc {
    [categoryList release];
    [super dealloc];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [categoryList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    LabelCell *cell = (LabelCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[LabelCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    cell.colorView.backgroundColor = [kCategoryColorArray objectAtIndex:(indexPath.row%[kCategoryColorArray count])];

    
    if (indexPath.row<[categoryList count]) {
        cell.contentLabel.text = [categoryList objectAtIndex:indexPath.row];
        [cell.contentLabel setTextColor:[UIColor blackColor]];
    } else {
        cell.contentLabel.text = @"﹢ 新增類別";
        [cell.contentLabel setTextColor:[UIColor redColor]];
    }
    
    UIView* myBackgroundView = [[UIView alloc] initWithFrame:CGRectZero] ;
    if (indexPath.row%2 == 0) {
        cell.contentView.backgroundColor = [UIColor colorWithRed:0.867 green:0.886 blue:0.91 alpha:1.0];
        cell.backgroundView = myBackgroundView;
        cell.backgroundView.backgroundColor = [UIColor colorWithRed:0.867 green:0.886 blue:0.91 alpha:1.0];
        [myBackgroundView release];
    } else {
        cell.contentView.backgroundColor = [UIColor colorWithRed:0.925 green:0.925 blue:0.945 alpha:1.0];    
        cell.backgroundView = myBackgroundView;
        cell.backgroundView.backgroundColor = [UIColor colorWithRed:0.925 green:0.925 blue:0.945 alpha:1.0];      
        [myBackgroundView release];        
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row<[categoryList count]) {
        UINavigationController *nvc =(UINavigationController*) [self.splitViewController.viewControllers objectAtIndex:1];
        CardManageViewController *vc = (CardManageViewController*)[nvc.viewControllers objectAtIndex:0];
        [vc reloadDataWithCardArray:[[DataBaseManager defaultDataBaseManager] getCardWithCategory:[categoryList objectAtIndex:indexPath.row]]];
        vc.title = [NSString stringWithFormat:@"編輯：%@",[categoryList objectAtIndex:indexPath.row]];
    }
}

- (void)refresh {
    self.categoryList = [[DataBaseManager defaultDataBaseManager] getAllCategory];
    [self.tableView reloadData];
}

@end
