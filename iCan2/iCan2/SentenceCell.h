//
//  SentenceCell.h
//  iCan
//
//  Created by avenc on 2011/6/12.
//  Copyright 2011年 NTU MHCI LAb . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "DataBaseManager.h"
#import "Sentence.h"
@interface SentenceCell : UITableViewCell {
    UIImageView *thumbImage;
    UILabel *label;
    Sentence *sentence;
}
@property (nonatomic, retain) UIImageView *thumbImage;
@property (nonatomic, retain) UILabel *label;
@property (nonatomic, retain)  Sentence *sentence;
- (id)initWithreuseIdentifier:(NSString *)reuseIdentifier;
+ (CGFloat)getCellHeight:(NSString*)str;
- (CGFloat)getLabelHeight;
- (void)setUpCell:(Sentence *)aSentence;
@end
