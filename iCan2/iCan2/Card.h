//
//  Card.h
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject {
    NSNumber *cid;
    NSString *title;
    NSString *soundPath;
    NSString *category;
    NSString *imagePath;
    NSData   *soundData;
    UIImage  *image;
}
@property (nonatomic,retain)NSNumber *cid;
@property (nonatomic,retain)NSString *title;
@property (nonatomic,retain)NSString *soundPath;
@property (nonatomic,retain)NSString *category;
@property (nonatomic,retain)NSString *imagePath;
@property (nonatomic,retain)NSData   *soundData;
@property (nonatomic,retain)UIImage  *image;
@end
