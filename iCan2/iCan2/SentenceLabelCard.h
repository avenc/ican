//
//  SentenceLabelCard.h
//  iCan
//
//  Created by avenc on 2011/6/12.
//  Copyright 2011年 NTU MHCI LAb . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Card.h"

@interface SentenceLabelCard : UIView {
    UIImageView *imageView;
    UILabel *label;
    int index;
    int cardId;
}
@property(nonatomic,retain)UIImageView *imageView;
@property(nonatomic,retain)UILabel *label;
@property(nonatomic,assign)int index;
@property(nonatomic,assign)int cardId;
- (id)initWithCard:(Card *)aCard andIndex:(int)aIndex;
@end
