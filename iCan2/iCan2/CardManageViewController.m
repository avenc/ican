//
//  CardManageViewController.m
//  iCan2
//
//  Created by 陳 雍協 on 11/9/22.
//  Copyright 2011年 政治大學. All rights reserved.
//

#import "CardManageViewController.h"
#import "CreateCardViewController.h"
@implementation CardManageViewController
@synthesize scrollView,pageControl,category;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 705, 768-44-44)];
    backgroundView.backgroundColor = kBackGroundColor;
    self.view = backgroundView;
    [backgroundView release];
    if (scrollView == nil) {
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(-1, 0, 705, 768-44-44)];
        scrollView.backgroundColor = [UIColor colorWithRed:0.906 green:0.906 blue:0.91 alpha:1.0];
        scrollView.pagingEnabled = YES;
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.scrollsToTop = NO;
        scrollView.delegate = self;
        [scrollView setClipsToBounds:NO];
        [scrollView setCanCancelContentTouches:NO];
    }
    
    if (pageControl == nil) {
        pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.scrollView.frame.size.height + 10.0, 705, 20.0)];
        pageControl.backgroundColor = [UIColor clearColor];
        pageControl.currentPage = 0;
        [pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
    }
    [self.view addSubview:scrollView];
    [self.view addSubview:pageControl];
    
    NSString *firstCategory = [[[DataBaseManager defaultDataBaseManager] getAllCategory] objectAtIndex:0];
    [self reloadDataWithCardArray:[[DataBaseManager defaultDataBaseManager]getCardWithCategory:firstCategory]];
    self.title = [NSString stringWithFormat:@"編輯：%@",firstCategory];
}


- (void)viewDidUnload {
    self.scrollView = nil;
    self.pageControl = nil;
    [super viewDidUnload];
}

- (void)dealloc
{
    [category release];
    [popover release];
    [scrollView release];
    [pageControl release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)changePage:(id)sender {
    int page = pageControl.currentPage;
    CGRect frame = scrollView.frame;
    frame.origin.x = 705 * page;
    frame.origin.y = 0;
    [scrollView scrollRectToVisible:frame animated:YES];
    pageControlUsed = YES;
}

- (void)reloadDataWithCardArray:(NSArray*)array {
    
    int cardCount = [array count]+1;
    
    if (cardCount/9 == 0) pages = cardCount/9;
    else pages = (cardCount/9) + 1;
    
    [scrollView scrollRectToVisible:CGRectMake(-1, 0, 705, 768-44-44) animated:NO];    
    scrollView.contentSize = CGSizeMake(705*pages+1, 768-44-44);
    pageControl.numberOfPages = pages;
    for (UIView *view in scrollView.subviews) {
        [view removeFromSuperview];
    }
    
    for (int i = 0; i<cardCount; i++) {
        int page = i/9;
        int indexInPage = i%9;
        CGPoint baseOrigin = CGPointMake(page*705, 0);
        baseOrigin.x = baseOrigin.x + (indexInPage%3)*kCardButtonWidth;
        baseOrigin.y = baseOrigin.y + (indexInPage/3)*kCardButtonHeight;
        
        
        if (i == 0) {
            CardButton *card = [[CardButton alloc] initWithCardStyle:CardButtonStyleAdd andCard:nil];
            card.delegate = self;
            CGRect frame = card.frame;
            frame.origin.x = baseOrigin.x;
            frame.origin.y = baseOrigin.y;
            
            card.frame = frame;
            [scrollView addSubview:card];
            [card release];
        } else {
            CardButton *card = [[CardButton alloc] initWithCardStyle:CardButtonStyleDefault andCard:[array objectAtIndex:i-1]];
            card.delegate = self;
            CGRect frame = card.frame;
            frame.origin.x = baseOrigin.x;
            frame.origin.y = baseOrigin.y;
            
            card.frame = frame;
            [scrollView addSubview:card];
            [card release];
        }
        
    }
}

#pragma mark -
#pragma mark UIScrollViewDelegate

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlUsed = NO;
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (pageControlUsed) return;
	
    // Switch the indicator when more than 50% of the previous/next page is visible
	CGFloat pageWidth = scrollView.frame.size.width;
	int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = page;
    
}

#pragma mark - CardButtonDelegate

-(void)CardButtonEditDidClicked:(CardButton*)cardButton {
    CreateCardViewController *createCardView = [[CreateCardViewController alloc] initWithNibName:@"CreateCardViewController" bundle:nil andCard:cardButton.card];
    createCardView.title = cardButton.card.title;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:createCardView];
    [self.parentViewController.parentViewController presentModalViewController:nav animated:YES];
    [createCardView release];
    [nav release];
}

-(void)CardButtonAddDidClicked:(CardButton*)cardButton {
    NSLog(@"XDD");
    Card *emptyCard = [[Card alloc] init];
    emptyCard.category = category;
    CreateCardViewController *createCardView = [[CreateCardViewController alloc] initWithNibName:@"CreateCardViewController" bundle:nil andCard:emptyCard];
    [emptyCard release];
//    createCardView.title = cardButton.card.title;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:createCardView];
    [self.parentViewController.parentViewController presentModalViewController:nav animated:YES];
    [createCardView release];
    [nav release];
}


@end
