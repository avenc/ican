//
//  LabelCell.m
//  iCan
//
//  Created by danielbas on 2011/5/19.
//  Copyright 2011年 NTU MHCI LAb . All rights reserved.
//

#import "LabelCell.h"


@implementation LabelCell
@synthesize contentLabel,colorView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        colorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 60)];
        [self addSubview:colorView];
        
        contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 320, 40)];
        contentLabel.font = [UIFont systemFontOfSize:18.0f];
        contentLabel.adjustsFontSizeToFitWidth = NO;
        contentLabel.numberOfLines = 0;
        contentLabel.backgroundColor = [UIColor clearColor];
        contentLabel.textAlignment = UITextAlignmentCenter;
        contentLabel.textColor = [UIColor blackColor];
        contentLabel.lineBreakMode = UILineBreakModeCharacterWrap;
        [self addSubview:contentLabel];
        
        self.accessoryType = UITableViewCellAccessoryNone;
        self.selectionStyle = UITableViewCellSelectionStyleBlue;
        self.contentLabel.backgroundColor = [UIColor clearColor];
        self.contentLabel.opaque = NO;
    }
    return self;
}

- (id)initWithIndex:(int)index reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        static NSMutableArray *colors = nil;
        UIView* myBackgroundView = [[UIView alloc] initWithFrame:CGRectZero] ;
        self.backgroundView = myBackgroundView;
        [myBackgroundView release];
        if (index%2 == 0) {
            colors = [[NSMutableArray alloc] initWithCapacity:4];
            [colors addObject:(id)[[UIColor colorWithRed:0.867 green:0.886 blue:0.91 alpha:1.0] CGColor]];
            [colors addObject:(id)[[UIColor colorWithRed:0.867 green:0.886 blue:0.94 alpha:1.0] CGColor]];
            [colors addObject:(id)[[UIColor colorWithRed:0.867 green:0.886 blue:0.96 alpha:1.0] CGColor]];
            [colors addObject:(id)[[UIColor colorWithRed:0.867 green:0.886 blue:0.98 alpha:1.0] CGColor]];
        } else {
            colors = [[NSMutableArray alloc] initWithCapacity:4];
            [colors addObject:(id)[[UIColor colorWithRed:0.925 green:0.925 blue:0.945 alpha:1.0] CGColor]];
            [colors addObject:(id)[[UIColor colorWithRed:0.945 green:0.925 blue:0.945 alpha:1.0] CGColor]];
            [colors addObject:(id)[[UIColor colorWithRed:0.965 green:0.925 blue:0.945 alpha:1.0] CGColor]];
            [colors addObject:(id)[[UIColor colorWithRed:0.985 green:0.925 blue:0.945 alpha:1.0] CGColor]];
        
        }
        [(CAGradientLayer *)self.backgroundView.layer setColors:colors];
        
        colorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 60)];
        [self addSubview:colorView];
        
        contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 320, 40)];
        contentLabel.font = [UIFont systemFontOfSize:18.0f];
        contentLabel.adjustsFontSizeToFitWidth = NO;
        contentLabel.numberOfLines = 0;
        contentLabel.backgroundColor = [UIColor clearColor];
        contentLabel.textAlignment = UITextAlignmentCenter;
        contentLabel.textColor = [UIColor blackColor];
        contentLabel.lineBreakMode = UILineBreakModeCharacterWrap;
        [self addSubview:contentLabel];
        
        self.accessoryType = UITableViewCellAccessoryNone;
        self.selectionStyle = UITableViewCellSelectionStyleGray;
        self.contentLabel.backgroundColor = [UIColor clearColor];
        self.contentLabel.opaque = NO;
    }
    return self;
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    if (highlighted) {
        contentLabel.textColor = [UIColor whiteColor];
    } else {
        contentLabel.textColor = [UIColor blackColor];
    }
    
}

- (void)dealloc
{
    [colorView release];
    [contentLabel release];
    [super dealloc];
}

@end
